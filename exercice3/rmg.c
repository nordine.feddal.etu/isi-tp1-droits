#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h> 
#include "check_pass.h"

void usage(){
	printf("./rmg [path/filename]"); 
}
int main(int argc, char ** argv){
    struct stat info;
    gid_t *group;
    int ngroups;
    long ngroups_max;
    int user_belong_file_group; 
    int file_exist; 
    if(argc <2){
	usage();
	exit(EXIT_FAILURE); 
    }
    // allow us to know if the user belong the file group 
    user_belong_file_group = -1; 
    // before asking username password
    // check if filename groupid is the same as rgid
    file_exist = stat(argv[1], &info);
    if(file_exist != 0){
	printf("THERE'S NO FILE %s\n", argv[1]); 
	exit(EXIT_FAILURE); 
    }
    
    ngroups_max = sysconf(_SC_NGROUPS_MAX) + 1;
    group = (gid_t *)malloc(ngroups_max *sizeof(gid_t));
    ngroups = getgroups(ngroups_max, group);
      
    for(int i = 0; i < ngroups;i++){
    	if(info.st_gid == group[i]){
	  	user_belong_file_group =1;
	        break;
	}	
    }
    if(user_belong_file_group == -1){
	printf("You're not belonging to the file owner group\n");
	exit(EXIT_FAILURE); 
    }

    char pass[30];
    // get the username from is real uid 
    struct passwd * pw; 
    pw = getpwuid(getuid());
    if(pw == NULL){
	perror("can't fetch username");
	exit(EXIT_FAILURE);
    }		   
    
    printf("Enter password: [max_length 30]\n");
    scanf("%29s", pass);
    if(check_pass(pw->pw_name,pass)){
	printf("You can delete the file\n");
        int del;
	del = remove(argv[1]); 
	if(!del){
	   printf("The file %s is deleted successfully\n",argv[1]); 
	   exit(EXIT_SUCCESS); 
	}else{
	   printf("Error\n");
	   exit(EXIT_FAILURE); 
	}
    }
    return EXIT_SUCCESS; 
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/types.h>
#include "suid.h"

void usage(){
    printf("./suid [filename]");
    exit(EXIT_FAILURE);     
}

int main(int argc, char *argv[])
{
    FILE * f; 
    // DISPLAY  
    if(argc < 2)
	usage(); 
    displayID();  
    f = fopen(argv[1],"r");
    if(f == NULL){
	perror("Cannot open file"); 
	exit(EXIT_FAILURE); 	
    }    
    printf("File opens correctly\n"); 
    readFile(f); 
    fclose(f); 
    return 0;
}


void displayID(){
    // DISPLAY  
    printf("Real UID\t= %d\n", getuid());
    printf("Effective UID\t= %d\n", geteuid());
    printf("Real GID\t= %d\n", getgid());
    printf("Effective GID\t= %d\n", getegid());
}

void readFile(FILE * file){
    char buffer[100]; 
    while(fgets(buffer,sizeof(buffer),file) != NULL){
	printf("%s",buffer); 
    }
}

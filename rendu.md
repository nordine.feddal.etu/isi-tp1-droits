# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Feddal, Nordine,  email: nordine.feddal.etu@univ-lille.fr

## Question 1
création de isitoto et affectation
```bash
sudo adduser isitoto
sudo usermod -a -G korede isitoto
```

Et voici le fichier myfile.txt avec les droits suivants.

L'utilisateur isitoto ne peut pas écrire dans le fichier myfile.txt.

Ceci s'explique de la manière suivante le programme vérifie d'abord si l'EUID du processus et celui du propiétaire du fichier correspondent. Si oui il prend le premier triplet.

Or ici l'EUID du processus correspond bien à celui du propriétaire du fichier isitoto, et le premier triplet est -r- donc isitoto ne peut que lire  le fichier myfile.txt.


## Question 2

Le flag x présent sur les répertoires, signifie que l'utilisateur qui est assigné à ce répertoire peut y entrer et lire et écrire dans les fichiers.

Création et modification
```bash
user korede:
mkdir mydir
chmod 761 mydir
ls -l
drwxrw---x 2 korede korede   4096 janv. 26 01:43 mydir


user isitoto:
cd /home/korede/isi-/mydir
```

user isitoto:
cd /home/korede/isi-/mydir



On a comme droits sur le fichier mydir **drwxrw---x 2 korede korede** , donc on a:  -
- pour l'utilisateur korede : rwx
- pour les utilisateurs du groupe korede: rw-
- Et pour les autres: --x



On ne peut pas entrer dans le dossier avec l'utilisateur isitoto car le système compare l'EUID du processus et celui du propriétaire. Ici ils sont différents, donc il compare l'EGID  et celui du fichier, la ils correspondent.

Or les utilisateurs du groupe korede autre que korede n'ont pas le droit d'exécution sur le répertoire, donc ils ne peuvent pas y entrer.

Après création du data.txt par l'utilisateur korede, on tente de lister le répertoire avec l'utilisateur isitoto

on obtient le message suivant:
```bash

ls: impossible d'accéder à '/home/korede/isi-tp1-droits/mydir/data.txt': Permission non accordée                                            
data.txt

```

On ne peut pas accéder au dossier mydir mais il affiche quand même le  document présent à l'intérieur.

Avec un ls -l on peut voir que les informations concernant les droits des fichiers sont cachés par le système.

```bash
ls -l /home/korede/isi-tp1-droits/mydir/
ls: impossible d'accéder à '/home/korede/isi-tp1-droits/mydir/data.txt': Permission non accordée
ls: impossible d'accéder à '/home/korede/isi-tp1-droits/mydir/test': Permission non accordée
total 0
-????????? ? ? ? ?              ? data.txt
-????????? ? ? ? ?              ? test
```
Nous n'avons pas le bit d'exécution pour le groupe korede sur le répertoire mydir, donc on ne peut pas accéder aux informations des fichiers présent à l'intérieur.

## Question 3


lorsqu'on lance la commande avec l'utilisateur isitoto on obtient la sortie suivante:
```bash
Real UID        = 1001
Effective UID   = 1001
Real GID        = 1001
Effective GID   = 1001  
Cannot open file: Permission denied
```

Normal car l'utilisateur est isitoto et que son id est 1001 et que le groupe isitoto est aussi 1001.

Si on utilise le set-user-id
```bash

chmod u+s setuid

```

et qu'on relance la commande avec isitoto on obtient en sortie:
```bash
Real UID        = 1001
Effective UID   = 1000
Real GID        = 1001
Effective GID   = 1001
File opens correctly
salut  <- lecture du fichier
```

Cette fois ci on arrive à ouvrir le fichier et à lire le contenu.
Cela est du au fait que le programme est lancé avec l'EUID 1000 qui est l'uid de l'utilisateur korede qui est propriétaire du dossier mydir et qui possède le bit d'éxécution sur ce dossier. Donc on arrive à accéder aux documents présent dans le dossier.



## Question 4


Lors de l'exécution du script python, même si le fichier possède le bit s de la fonction set-user-id, les id restent les mêmes voici ce qu'on obtient en sortie:

```bash
python3 suid.py
1001
1001
1001
1001
```

## Question 5

chfn - permet de modifier le nom complet et les informations associées à un utilisateur  

```bash
-rwsr-xr-x 1 root root 85064 mai   28  2020 /usr/bin/chfn
```

Donc ici on a le set-user-id flag de mit en s. On a aussi en other r-x comme droit donc tout le monde peut éxecuter ce programme.

Or comme on a la flag s dans les droits utilisateur qui est root, n'importe qui peut exécuter ce programme avec les priviléges de l'utilisateur root.

LIGNE de l'utilisateur isitoto dans le fichier /etc/passwd avant modification:
```bash
isitoto:x:1001:1001:,,,:/home/isitoto:/bin/bash
```

après modification:
```bash

isitoto:x:1001:1001:,test,test,test:/home/isitoto:/bin/bash

```

## Question 6


Les mots de passes sont stocké dans le fichier /etc/shadow, qui à ces droits:
```bash
-rw-r----- 1 root shadow 1570 janv. 26 01:27 /etc/shadow
```
Les mots de passe sont stocké dans ce fichier et encrypté, ce fichier à l'avantage de ne pas être lisible par tout le monde contrairement au fichier /etc/passwd dont voici les droits.

```bash
-rw-r--r-- 1 root root 2801 janv. 26 02:52 /etc/passwd
```

le set_user_id permet que lorsqu'un utilisateur crée un fichier dans le répertoire, le fichier à pour groupe celui du répertoire ou il a été créé.
Dans le fichier ayant pour groupe korede.
```bash
ls -l                                                            
total 0                                                                                            
-rw-rw-r-- 1 isitoto korede  0 janv. 26 03:06 isitotofiletest2.txt         <- créé après set_group_id chmod g+s
-rw-rw-r-- 1 isitoto isitoto 0 janv. 26 03:05 isitotofile.txt              <- créé avant set_group_id chmod g+s
-rw-rw-r-- 1 korede  korede  0 janv. 26 03:05 koredefile.txt    
```

## Question 7
### Notes importantes:
### groupe_a = korede
### groupe_b = isitoto

Mettre les scripts bash dans le repertoire *question7*.

On crée les répertoires avec l'utilisateurs admin
```bash
mkdir dir_a dir_b  dir_c

chown :korede dir_a
chown :isitoto dir_b
```
on leurs applique le set_group_id pour que les fichiers créer à l'intérieur du répertoire par n'importe quel utilisateur du groupe korede ou isitoto soit affilié au groupe korede ou isitoto.

```bash
chmod g+s dir_korede_a
chmod g+s dir_isitoto_b
```

on ajoute le sticky bit pour empécher de supprimer ou raname un fichier qui n'appartient pas au propriétaire.
```bash
chmod +t dir_a
chmod +t dir_b
```
### Remarques:
Le UMASK est fixé à 022  par défaut lorsqu'un fichier est créé, il sera créé avec les droits suivant
644 (rw- r– r–)
on ne pourra donc pas modifier les fichiers  d'autre utilisateurs appartenant au même groupe  sans modifier le masque.
pour cela on peut soit modifier temporairement le umask comme suit avec la commande:
```bash
umask  002
```
ou bien modifier sa valeur dans le fichier **/etc/login.defs**.

**BUG** avec ubuntu même en modifiant le umask cela ne fonctionne pas si le sticky bit est présent les utilisateurs du groupes ne peuvent pas écrire sur les fichiers des membres du groupe.

Je passe donc sur une machine debian 9, ou la ca marche avec les mêmes manipulations...
## Question 8

Pour la question 8, le code et les scripts bash se trouvent dans la répertoire question 8.

Pour compiler rmg.
```bash
make
```

Puis il faut changer le owner du fichier à admin.
```bash
chown admin rmg
```
Enfin il faut lui donné le set_user_id
```bash
chmod u+s rmg
```
Pour exécuter les scripts:
```bash
bash delete_file_group_a.sh
bash delete_file_group.b.sh
```
### Remarques

Les scripts **delete_file_group_a.sh** et **delete_file_group_b**, ne demande pas le mot de passe de l'utilisateur.
Il utilise le mot de passe "test$USER".
Donc si l'utilisateur qui lance le script est korede le mot de passe qui sera automatiquement utilisé par les scripts sera "testkorede".
Il faut donc modifié les mots de passes dans le fichier /home/admin/passwd, au préalable ou utiliser la fonction pwg pour changer le mot de passe.

Ou encore utilisé les scripts qui demande le mot de passe **delete_file_group_a_asking_password.sh** et **delete_file_group_b_asking_password.sh**.

## Question 9
Pour la question 9, le code et les scripts bash se trouvent dans la répertoire question 9.

Pour compiler pwg.
```bash
make
```

Puis il faut changer le owner du fichier à admin.
```bash
chown admin pwg
```
Enfin il faut lui donné le set_user_id
```bash
chmod u+s pwg
```

Pour exécuter les scripts:
```bash
bash pwg_test_korede.sh
bash pwg_test_isitoto.sh
```
Les tests ce déroulent de la manière suivante:
on modifie le mot de passe de l'utilisateur korede ou isitoto, on part du principe que le mot de passe de base est le nom de l'utilisteur.
On modifie son mot de passe à l'aide de la commande pwg, en test$USER.
Puis on lance le script bash de la question 8 pour voir si il arrive bien à supprimer un fichier dans le dossier dir_a ou dir_b avec le mot de passe que l'on vient de mettre test\$USER.
Puis on remodifie le mot de passe en lui donnant le nom de l'utilisateur.
## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.

#ifndef CHECK_PASS_H
#define CHECK_PASS_H
struct user{
	char name[30];
	char pass[30]; 
};

/* This function allows us to check if a given password
 * is the good one for a specific username
 *
 * @return:
 * int 				0 if true, 1 otherwise
 *
 */
int check_pass(const char * username,const  char * pass); 

/* This function allows us to read the file 
 * /home/admin/passwd and create an array of all 
 * user already registered
 *
 * @return:
 * int 				the number of user already registered
 */
int read_file(); 

/* This function allows us to know if the username
 * is already registered in passwd file
 * 
 * @param:
 * const char * username	the username
 *
 * @return:
 * int 				0 if the username is already registered, 1 otherwise
 */
int exist_entry(const char * username); 

/* This function allows us to get he current user
 *
 * @return:
 * struct user 		the curent user
 */
struct user get_cur_user(); 
#endif 

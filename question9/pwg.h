#ifndef PWG_H
#define PWG_H

/* This method allows us to modify a line in a file
 * this method process using a temporary file 
 * write the same line in the new temporary file if it's not the one we want to change
 * otherwise write the modified line
 * at the end the function delete the current file and rename the temporary file to the original file
 *
 * @param:
 * const char * newpass		the new password 
 * const char * actualPass	the actual password
 *
 * @return:
 * int				0 if we modify line			
 */
int write_new_password(const char * newpass, const char * actualPass);  

#endif 

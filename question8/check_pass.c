#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <crypt.h>
#include <string.h>
#include "check_pass.h"
static struct user *  registred_users;
struct user  cur_user; 
int check_pass(const char * username, const char * pass){
	int nb_line; 
	char * encrypted_pass = crypt(pass,"tx"); 
	nb_line = read_file();
	for(int i = 0; i < nb_line;i++){
	 	if(strcmp(username,registred_users[i].name) == 0){
			if(strcmp(encrypted_pass,registred_users[i].pass) == 0){
				free(registred_users); 
				return 0; 
			}
		}
	}
	free(registred_users); 
	return 1; 
}

int read_file(){
	FILE * f;
	int count_line; 
	char c;
	count_line = 0; 
	f = fopen("/home/admin/passwd","r");
       	if(f == NULL){
		perror("Erreur lors de l'ouverture du fichier");
		exit(EXIT_FAILURE); 
	}	
       	c = fgetc(f); 	
	// count number of line
	while(!feof(f)){
	     if(c == '\n')
	 	count_line++;
	     c=fgetc(f); 
	}
	// retour au début du fichier
	fseek(f,0,SEEK_SET); 
	registred_users = malloc(sizeof(struct user)*count_line);
	for(int i =0; i < count_line;i++){
		fscanf(f,"%s %s", registred_users[i].name, registred_users[i].pass);
	}
	fclose(f); 
	return count_line; 
}

int exist_entry(const char * username){
	int nb_line; 
	nb_line = read_file();
	for(int i = 0; i < nb_line;i++){
	 	if(strcmp(username,registred_users[i].name) == 0){
			cur_user = registred_users[i]; 
			free(registred_users);
			return 0;  
		}
	}
	free(registred_users);
	return 1; 	
}

struct user  get_cur_user(){
	return cur_user; 
}

#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <crypt.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <string.h>
#include "check_pass.h"
#include "pwg.h"
#define MAX 60
int main(){

    struct passwd * pw;
    char actualPass[30]; 
    char newPass[30];  
    char verifNewPass[30];
    newPass[0] ='a';
    verifNewPass[0] ='b';

    int change_chance; 
    change_chance =  0; 
    // for the test
    pw = getpwuid(getuid());
    if(pw == NULL){
	perror("can't fetch username");
	exit(EXIT_FAILURE);
    }
    if(exist_entry(pw->pw_name) == 0){
	struct user cur_user; 	
	cur_user = get_cur_user();
        while(change_chance !=3){	
    		printf("You're already registred please enter your actual password : ");
        	scanf("%29s",actualPass);
		char * encrypted_pass = crypt(actualPass,"tx");
		printf("%s\n",encrypted_pass);
		if((strcmp(encrypted_pass,cur_user.pass) == 0) || (strcmp(actualPass,cur_user.pass) == 0)){
			printf("Ok you can change your password\n"); 
			// ask until the new password is verified
			while(strcmp(newPass,verifNewPass) != 0){
				printf("Please enter new password : ");
				scanf("%29s", newPass); 
				printf("Verification new password : ");
				scanf("%29s", verifNewPass); 
			}
			write_new_password(newPass,actualPass);
		        break; 	
		}
	change_chance++; 
    	printf("Error %d chance left.\n ", 3 - change_chance);
	}
    }
    else{
	FILE * f; 
	f=fopen("/home/admin/passwd","a"); 
        char new_entry[MAX]; 
	strcpy(new_entry,pw->pw_name); 
	strcat(new_entry," ");
	while(strcmp(newPass,verifNewPass) != 0){
		printf("Please enter new password : ");
		scanf("%29s", newPass); 
		printf("Verification new password : ");
		scanf("%29s", verifNewPass); 
	}
	char * encrypt_pass = crypt(newPass,"tx"); 
	strcat(new_entry,encrypt_pass);
	strcat(new_entry,"\n");
	fprintf(f,"%s",new_entry); 
        fclose(f); 	
    }
    return 0; 
}

int write_new_password(const char * newpass,const char * actualPass){
        FILE *fp1, *fp2;
        char string[MAX];
        char newline[MAX], temp[] = "/home/admin/temp.txt";
    	char pastline[MAX];
        fp1 = fopen("/home/admin/passwd", "r+");
	char * encryptedActualPass;
        encryptedActualPass= crypt(actualPass,"tx");

        if (!fp1) {
                printf("Unable to open the input file!!\n");
                return 0;
        }

        fp2 = fopen(temp, "w");

        if (!fp2) {
                printf("Unable to open a temporary file to write!!\n");
                fclose(fp1);
                return 0;
        }
	struct user cur_user;
        cur_user = get_cur_user(); 
	// to compare with pastline
	strcpy(pastline,cur_user.name); 
 	strcat(pastline, " "); 
	strcat(pastline, encryptedActualPass);
	strcat(pastline, "\n");

        while (!feof(fp1)) {
                strcpy(string, "\0");
                fgets(string, MAX, fp1);
                if (!feof(fp1)) {
                        if (strcmp(pastline,string)==0) {
				char * encryptedPass = crypt(newpass,"tx");
				strcpy(newline,cur_user.name); 
 				strcat(newline, " "); 
				strcat(newline, encryptedPass);
				strcat(newline, "\n");
                                fprintf(fp2, "%s", newline);
                        } else {
                                fprintf(fp2, "%s", string);
                        }
                }
        }

        fclose(fp1);
        fclose(fp2);
        remove("/home/admin/passwd");
        rename(temp, "/home/admin/passwd");
    	return 0;
}
